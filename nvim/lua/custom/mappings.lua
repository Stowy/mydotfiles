local M = {}

M.dap = {
	n = {
		["<leader>db"] = {
			"<cmd> DapToggleBreakpoint <CR>",
			"Toggle breakpoint"
		},
		["<leader>dr"] = {
			"<cmd> DapContinue <CR>",
			"Start or continue the debugger"
		},
		["<leader>dus"] = {
			function ()
				local widgets = require("dap.ui.widgets");
				local sidebar = widgets.sidebar(widgets.scopes);
				sidebar.open();
			end,
			"Open debuggin sidebar"
		}
	}
}

M.crates = {
	n = {
		["<leader>rcu"] = {
			function ()
				require("crates").upgrade_all_crates()
			end,
			"Update crates"
		}
	}
}

M.rustacean = {
	n = {
		["<leader>a"] = {
			function ()
				vim.cmd.RustLsp('codeAction')
			end,
			"Show rust code action"
		}
	}
}

return M
