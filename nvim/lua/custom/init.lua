if vim.g.neovide then
  vim.o.guifont = "CaskaydiaCove Nerd Font:h11"
  vim.g.neovide_scale_factor = 1.0
end

vim.o.expandtab = false
vim.opt.tabstop = 4
vim.opt.softtabstop = 0
vim.opt.shiftwidth = 0

local is_windows = package.config:sub(1, 1) == '\\'

if is_windows then
	vim.o.shell = "C:/Program Files/Git/bin/bash.exe"
	vim.o.shellcmdflag = "-c"
end

-- NvChad specific file type configuration
vim.cmd([[
	augroup NvChadFileTypeGLSL
		autocmd!
		autocmd BufRead,BufNewFile *.vert,*.frag set filetype=glsl
	augroup END
]])

vim.cmd([[
	augroup NvChadFileTypeMDX
		autocmd!
		autocmd BufRead,BufNewFile *.mdx, set filetype=markdown
	augroup END
]])